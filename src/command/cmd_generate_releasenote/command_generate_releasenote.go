package cmd_generate_releasenote

import (
	"fmt"
	"os"
	"showmeyourcode/cra-generator/src/custom_io_tool"
	"showmeyourcode/cra-generator/src/html_template"
	"showmeyourcode/cra-generator/src/json_helper"
	"showmeyourcode/cra-generator/src/path_finder"
)

type Release struct {
	version string
	date    string
	fixed   []string
	new     []string
	removed []string
	name    string
}

type CommandGenerateReleaseNote struct {
	savePath string
}

func (c *CommandGenerateReleaseNote) Execute(workingDirectory string) {
	var releaseNoteSource = path_finder.GetReleaseNoteFolderPath(workingDirectory)
	var folder = custom_io_tool.GetFolderContent(releaseNoteSource)
	var releases []Release
	for i := len(folder.Subfolders) - 1; i >= 0; i-- {
		fmt.Println("Preparing release - " + folder.Name)
		releases = append(releases, prepareRelease(folder.Subfolders[i]))
	}
	var version = "?.?.?"
	if len(releases) > 0 {
		version = releases[0].version
	}
	saveDocument(createHtmlDocument(releases, workingDirectory), version, c)
}

func (c *CommandGenerateReleaseNote) PrintInfo() {
	fmt.Println("Generated document you will find under: " + c.savePath)
	fmt.Println("Finish preparing the latest Release Note document!")
}

func createHtmlDocument(documents []Release, workingDirectory string) string {
	var title = fmt.Sprintf("<PLATFORM-NAME> %s Release note | %s", documents[0].version, documents[0].date)
	var projectMetadata = getProjectMetadata(workingDirectory)
	var html = html_template.MyHtmlTemplate{}
	html.Title = title
	html.Elements = append(html.Elements,
		html_template.HtmlElement{Tag: html_template.HEADER,
			Content: html_template.ToStringHtmlElement(
				html_template.HtmlElement{Tag: html_template.H1, Content: projectMetadata["full-name"]}) +
				html_template.ToStringHtmlElement(
					html_template.HtmlElement{Tag: html_template.P, Content: projectMetadata["description"]})})
	for i := 0; i < len(documents); i++ {
		var document = documents[i]
		fmt.Println("Creating html entry in document for " + document.version)
		html.Elements = append(html.Elements, prepareReleaseHtmlArticle(documents[i])...)
	}
	return html_template.Render(html)
}

func getProjectMetadata(workingDirectory string) map[string]string {
	var _, metadata = json_helper.ParseJsonObject(path_finder.CombinePath(path_finder.GetComponentsDescriptionFolderPath(workingDirectory), "project.json"))
	return metadata
}

func prepareReleaseHtmlArticle(release Release) []html_template.HtmlElement {
	var result []html_template.HtmlElement
	result = append(result, html_template.HtmlElement{
		Tag:     html_template.H2,
		Content: release.version + " | " + release.date,
	})
	var nestedElements []html_template.HtmlElement
	nestedElements = parseComponentReleaseJsonFiles(release, nestedElements)
	result = append(result, html_template.WrapContentWithTag(html_template.ARTICLE, html_template.ToStringHtmlElements(nestedElements)))
	return result
}

func parseComponentReleaseJsonFiles(component Release, nestedElements []html_template.HtmlElement) []html_template.HtmlElement {
	if component.new != nil && len(component.new) > 0 {
		var listElements []html_template.HtmlElement
		nestedElements = append(nestedElements, html_template.HtmlElement{
			Tag:     html_template.H4,
			Content: "New",
		})
		for i := 0; i < len(component.new); i++ {
			listElements = append(listElements, html_template.HtmlElement{
				Tag:     html_template.LIST_ENTRY,
				Content: component.new[i],
			})
		}
		var bulletList = html_template.WrapContentWithTag(html_template.BULLET_LIST, html_template.ToStringHtmlElements(listElements))
		nestedElements = append(nestedElements, bulletList)
	}

	if component.fixed != nil && len(component.fixed) > 0 {
		var listElements []html_template.HtmlElement
		nestedElements = append(nestedElements, html_template.HtmlElement{
			Tag:     html_template.H4,
			Content: "Fixed",
		})
		for i := 0; i < len(component.fixed); i++ {
			listElements = append(listElements, html_template.HtmlElement{
				Tag:     html_template.LIST_ENTRY,
				Content: component.fixed[i],
			})
		}
		var bulletList = html_template.WrapContentWithTag(html_template.BULLET_LIST, html_template.ToStringHtmlElements(listElements))
		nestedElements = append(nestedElements, bulletList)
	}

	if component.removed != nil && len(component.removed) > 0 {
		var listElements []html_template.HtmlElement
		nestedElements = append(nestedElements, html_template.HtmlElement{
			Tag:     html_template.H4,
			Content: "Removed",
		})
		for i := 0; i < len(component.removed); i++ {
			listElements = append(listElements, html_template.HtmlElement{
				Tag:     html_template.LIST_ENTRY,
				Content: component.removed[i],
			})
		}
		var bulletList = html_template.WrapContentWithTag(html_template.BULLET_LIST, html_template.ToStringHtmlElements(listElements))
		nestedElements = append(nestedElements, bulletList)
	}
	return nestedElements
}

func prepareRelease(directory custom_io_tool.Directory) Release {
	var releaseDate, version = prepareReleaseBasicInfo(path_finder.CombinePath(directory.Path, directory.Files[0]))
	fmt.Println("Preparing release: " + directory.Name)
	var release = Release{
		version: version,
		date:    releaseDate,
	}
	parseChanges(directory, &release)
	return release
}

func prepareReleaseBasicInfo(path string) (string, string) {
	var _, a = json_helper.ParseJsonObject(path)
	return a["release-date"], a["version"]
}

func parseChanges(directory custom_io_tool.Directory, release *Release) {

	for i := 0; i < len(directory.Files); i++ {
		var name, elements = json_helper.ParseJsonArray(path_finder.CombinePath(directory.Path, directory.Files[i]))
		var entries []string
		for i := 0; i < len(elements); i++ {
			entries = append(entries, elements[i]["description"])
		}
		switch name {
		case "fixed":
			release.fixed = entries
			break
		case "new":
			release.new = entries
			break
		case "removed":
			release.removed = entries
			break
		}
	}
}

func saveDocument(document string, version string, command *CommandGenerateReleaseNote) {
	command.savePath = path_finder.GetReleaseNoteFolderOutputPath(version, "")
	var rootDirectory, _ = path_finder.GetPathLastElement(command.savePath)
	os.MkdirAll(rootDirectory, os.ModePerm)
	custom_io_tool.SaveFile(command.savePath, document)
}
