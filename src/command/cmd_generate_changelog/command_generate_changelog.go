package cmd_generate_changelog

import (
	"fmt"
	"os"
	"showmeyourcode/cra-generator/src/custom_io_tool"
	"showmeyourcode/cra-generator/src/html_template"
	"showmeyourcode/cra-generator/src/json_helper"
	"showmeyourcode/cra-generator/src/path_finder"
)

type ChangelogChange struct {
	description   string
	referenceLink string
	referenceId   string
}

type ChangelogVersionChanges struct {
	name    string
	changes []ChangelogChange
}

type ChangelogVersion struct {
	version     string
	releaseDate string
	changes     []ChangelogVersionChanges
}

type Changelog struct {
	componentName string
	description   string
	versions      []ChangelogVersion
}

type CommandGenerateChangelog struct {
	savePaths []string
}

func (c *CommandGenerateChangelog) Execute(workingDirectory string) {
	fmt.Println("Executing command with source: " + workingDirectory)
	var changelogSource = path_finder.GetChangelogFolderPath(workingDirectory)
	var folder = custom_io_tool.GetFolderContent(changelogSource)
	for i := 0; i < len(folder.Subfolders); i++ {
		var component = folder.Subfolders[i]
		processComponent(component, workingDirectory, c)
	}
}

func (c *CommandGenerateChangelog) PrintInfo() {
	fmt.Println("Generated documents you will find under: ")
	var generatedDocumentPaths string
	for _, path := range c.savePaths {
		generatedDocumentPaths += fmt.Sprintf("%s\n", path)
	}
	fmt.Print(generatedDocumentPaths)
	fmt.Println("Finish preparing the latest Changelog documents for all the components-description!")
}

func processComponent(directory custom_io_tool.Directory, workingDirectory string, c *CommandGenerateChangelog) {
	fmt.Println("Processing component: " + directory.Name)
	var componentDetailsMetadata = getComponentDetailsMetadata(directory.Name, workingDirectory)
	var componentDescriptionMetadata = getComponentDescriptionsMetadata(directory.Name, workingDirectory)
	var latestVersion = getLatestVersion(directory)
	var documentContent = generateChangelogDocument(componentDescriptionMetadata, componentDetailsMetadata, directory, latestVersion)
	saveChangelogDocument(documentContent, directory.Name, latestVersion, c)
}

func getLatestVersion(directory custom_io_tool.Directory) string {
	return directory.Subfolders[len(directory.Subfolders)-1].Name
}

func getComponentDetailsMetadata(name string, workingDirectory string) map[string]string {
	_, json := json_helper.ParseJsonObject(path_finder.CombinePath(path_finder.CombinePath(path_finder.GetComponentsDescriptionFolderPath(workingDirectory), name), "details.json"))
	return json
}

func getComponentDescriptionsMetadata(name string, workingDirectory string) map[string]string {
	_, json := json_helper.ParseJsonObject(path_finder.CombinePath(path_finder.CombinePath(path_finder.GetComponentsDescriptionFolderPath(workingDirectory), name), "description.json"))
	return json
}

func saveChangelogDocument(content string, componentName string, version string, c *CommandGenerateChangelog) {
	var path = path_finder.GetChangelogFolderOutputPath(componentName, version, "")
	c.savePaths = append(c.savePaths, path)
	var rootDirectory, _ = path_finder.GetPathLastElement(path)
	os.MkdirAll(rootDirectory, os.ModePerm)
	custom_io_tool.SaveFile(path, content)
}

func generateChangelogDocument(componentDescriptionMetadata map[string]string, componentDetailsMetadata map[string]string, directory custom_io_tool.Directory, latestVersion string) string {
	fmt.Println("Generating changelog document...")
	var html = html_template.MyHtmlTemplate{}
	html.Title = fmt.Sprintf("<PLATFORM-NAME> Platform - %s %s Changelog ", directory.Name, latestVersion)
	html.Elements = append(html.Elements,
		html_template.HtmlElement{Tag: html_template.HEADER,
			Content: html_template.ToStringHtmlElement(
				html_template.HtmlElement{Tag: html_template.H1, Content: componentDescriptionMetadata["name"]}) +
				html_template.ToStringHtmlElement(html_template.HtmlElement{
					Tag:     html_template.P,
					Content: html_template.ToStringHtmlElements(prepareHeaderInfoForTemplate(componentDescriptionMetadata, componentDetailsMetadata))})})
	for i := 0; i < len(directory.Subfolders); i++ {
		var componentVersion = directory.Subfolders[i]
		fmt.Println("Processing version " + componentVersion.Name)
		var _, versionDetails = json_helper.ParseJsonObject(path_finder.CombinePath(componentVersion.Path, componentVersion.Files[0]))
		var changesDir = componentVersion.Subfolders[0]
		html.Elements = append(html.Elements, html_template.HtmlElement{
			Tag:     html_template.H2,
			Content: versionDetails["version"] + " | " + versionDetails["release-date"],
		})
		for j := 0; j < len(changesDir.Files); j++ {
			var fileName, content = json_helper.ParseJsonArray(
				path_finder.CombinePath(changesDir.Path, changesDir.Files[j]))
			html.Elements = append(html.Elements, parseChangelogChange(fileName, content)...)
		}
	}
	return html_template.Render(html)
}

func prepareHeaderInfoForTemplate(componentDescriptionMetadata map[string]string, componentDetailsMetadata map[string]string) []html_template.HtmlElement {
	var elements []html_template.HtmlElement
	elements = append(elements, html_template.HtmlElement{
		Tag:     html_template.DIV,
		Content: "<b>Description: </b>" + componentDescriptionMetadata["full-description"],
	})
	elements = append(elements, html_template.HtmlElement{
		Tag:     html_template.DIV,
		Content: "<b>Purpose: </b>" + componentDescriptionMetadata["purpose"],
	})
	elements = append(elements, html_template.HtmlElement{
		Tag:     html_template.DIV,
		Content: "<b>Initial release: </b>" + componentDescriptionMetadata["initial-release"],
	})
	elements = append(elements, html_template.HtmlElement{
		Tag:     html_template.DIV,
		Content: fmt.Sprintf("<b>Git url: </b> <a href=\"%s\" target=\"_blank\">%s</a>", componentDetailsMetadata["git-repository"], componentDetailsMetadata["git-repository"]),
	})
	elements = append(elements, html_template.HtmlElement{
		Tag:     html_template.DIV,
		Content: "<b>Main technology: </b>" + componentDetailsMetadata["main-technology"],
	})
	return elements
}

func parseChangelogChange(fileName string, json []map[string]string) []html_template.HtmlElement {
	var htmlElements []html_template.HtmlElement
	switch fileName {
	case "added":
		htmlElements = append(htmlElements, html_template.HtmlElement{
			Tag:     html_template.H3,
			Content: "Added",
		})
		break
	case "breaking-changes":
		htmlElements = append(htmlElements, html_template.HtmlElement{
			Tag:     html_template.H3,
			Content: "Breaking changes",
		})
		break
	case "changed":
		htmlElements = append(htmlElements, html_template.HtmlElement{
			Tag:     html_template.H3,
			Content: "Changed",
		})
		break
	case "deprecated":
		htmlElements = append(htmlElements, html_template.HtmlElement{
			Tag:     html_template.H3,
			Content: "Deprecated",
		})
		break
	case "fixed":
		htmlElements = append(htmlElements, html_template.HtmlElement{
			Tag:     html_template.H3,
			Content: "Fixed",
		})
		break
	case "removed":
		htmlElements = append(htmlElements, html_template.HtmlElement{
			Tag:     html_template.H3,
			Content: "Removed",
		})
		break
	case "security":
		htmlElements = append(htmlElements, html_template.HtmlElement{
			Tag:     html_template.H3,
			Content: "Security",
		})
		break
	}
	var listElements []html_template.HtmlElement
	for i := 0; i < len(json); i++ {
		listElements = append(listElements, html_template.HtmlElement{
			Tag:     html_template.LIST_ENTRY,
			Content: fmt.Sprintf("%s  %s %s", json[i]["reference-id"], json[i]["description"], json[i]["reference-link"])})
	}
	var bulletList = html_template.WrapContentWithTag(html_template.BULLET_LIST, html_template.ToStringHtmlElements(listElements))
	return append(htmlElements, bulletList)
}
