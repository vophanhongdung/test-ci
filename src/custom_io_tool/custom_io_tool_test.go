package custom_io_tool

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestGetFolderContent(t *testing.T) {
	const path = "../../examples"
	var folderContent = GetFolderContent(path)
	assert.Equal(t, "examples", folderContent.Name)
	assert.Equal(t, path, folderContent.Path)
	assert.Equal(t, 3, len(folderContent.Subfolders))
}

func TestLoadFileContent(t *testing.T) {
	const path = "../../examples/adr/template.md"
	var content = LoadFileContent(path)
	assert.NotNil(t, content)
}

func TestSaveFile(t *testing.T) {
	const path = "./test-save-output/test.txt"
	SaveFile(path, "Something something\nSomething new line")

	var content = LoadFileContent(path)
	assert.NotNil(t, content)
}
