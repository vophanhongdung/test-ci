package custom_io_tool

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"showmeyourcode/cra-generator/src/path_finder"
)

type Directory struct {
	Name       string
	Path       string
	Files      []string
	Subfolders []Directory
}

func GetFolderContent(path string) Directory {
	var _, lastPathElem = path_finder.GetPathLastElement(path)
	var result = Directory{
		lastPathElem,
		path,
		[]string{},
		[]Directory{},
	}
	files, err := ioutil.ReadDir(path)
	if err != nil {
		log.Fatal(err)
	}

	for _, f := range files {
		if f.IsDir() {
			fmt.Println("Processing folder: " + f.Name())
			result.Subfolders = append(result.Subfolders, GetFolderContent(path_finder.CombinePath(path, f.Name())))
		} else {
			fmt.Println("Found file: " + f.Name())
			result.Files = append(result.Files, f.Name())
		}
	}
	return result
}

func LoadFileContent(path string) string {
	file, err := os.Open(path)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	b, err := ioutil.ReadAll(file)
	return string(b)
}

func SaveFile(path string, fileContent string) {
	f, err := os.Create(path)
	if err != nil {
		fmt.Println(err)
		return
	}
	l, err := f.WriteString(fileContent)
	if err != nil {
		fmt.Println(err)
		f.Close()
		return
	}
	fmt.Println(l, "bytes written successfully")
	err = f.Close()
	if err != nil {
		fmt.Println(err)
		return
	}
}
