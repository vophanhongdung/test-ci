package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"showmeyourcode/cra-generator/src/command"
	"showmeyourcode/cra-generator/src/command/cmd_generate_changelog"
	"showmeyourcode/cra-generator/src/command/cmd_generate_releasenote"
	"showmeyourcode/cra-generator/src/constant"
	"showmeyourcode/cra-generator/src/path_finder"
	"strconv"
	"strings"
)

func main() {
	options := []string{"Release note", "Changelog"}
	reader := bufio.NewReader(os.Stdin)
	fmt.Println(fmt.Sprintf("Welcome in the documentation tool for <PLATFORM-NAME> platform! Current version: %s", constant.Version))
	fmt.Println("If you want to exit the program, please press '0'.")
	fmt.Println("\nSelect action:")
	for index, option := range options {
		fmt.Println(fmt.Sprintf("%d. %s", index+1, option))
	}
	isActionChosen := false
	var sourcePath = ""
	var commandToExecute command.Command = nil
chooseActionLoop:
	for !isActionChosen {
		userChoice, _ := reader.ReadString('\n')
		userChoice = strings.TrimSuffix(userChoice, "\n")
		choiceParsed, _ := strconv.Atoi(userChoice)
		switch choiceParsed {
		case 0:
			fmt.Println("Exit program!")
			break chooseActionLoop
		case 1:
			fmt.Println("Preparing Release notes...")
			commandToExecute = &cmd_generate_releasenote.CommandGenerateReleaseNote{}
			sourcePath = path_finder.GetWorkingDirectoryPath()
			isActionChosen = true
			break
		case 2:
			fmt.Println("Preparing Changelogs...")
			commandToExecute = &cmd_generate_changelog.CommandGenerateChangelog{}
			sourcePath = path_finder.GetWorkingDirectoryPath()
			isActionChosen = true
			break
		default:
			log.Fatal("Action not supported!")
		}
	}
	commandToExecute.Execute(sourcePath)
	commandToExecute.PrintInfo()
}
